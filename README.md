# Open Telemetry

## Configure ECK Integration

### Create APM API Key

- Kibana ➔ Observability ➔ APM ➔ Settings (top right) ➔ Agent Keys (top menu) ➔ Create APM agent key
- Set name = `otel-collector`, assign both read and write privileges
- Create secret containing the API key for otel collector

```bash
read -s apm_api_key

kubectl -n otel create secret generic "elastic-apm-api-key" --from-literal=api-key="${apm_api_key}" --dry-run=client -o yaml | kubectl apply --server-side=true -f -
```

### Configure Java Instrumentation

- Apply `java-instrumentation.yaml` in the app's namespace
- Add annotations to the app and environment variables to the app

```yaml
apiVersion: apps/v1
kind: Deployment
spec:
  template:
    metadata:
      annotations:
        instrumentation.opentelemetry.io/inject-java: "true"
    spec:
      containers:
      - env:
        - name: OTEL_RESOURCE_ATTRIBUTES
          value: deployment.environment=dev,service-namespace=dev
        - name: OTEL_JAVAAGENT_DEBUG
          value: "true"
        - name: OTEL_LOGS_EXPORTER
          value: otlp
```